const {createDeck,shuffle,draw, showCards, endTurn, endGame, minimax} = require('./sushi-go')
const deck = createDeck()
const prompt = require('prompt')
prompt.start()
shuffle(deck)
const player = {points:0 ,puddings:0,cards:[],hand:0}
const ai = {points:0 ,puddings:0,cards:[],hand:1}
hands= [[],[]]
const cardPerRound = 7
let input
async function play(){
    for(let i = 0;i<3;i++){
        console.log(`###### Round ${i+1} ######`)
        draw(deck,cardPerRound,hands[0],hands[1])
        for (let j = 0;j<cardPerRound;j++){
            console.log(`------ Draw ${j+1} ------`)
            console.log('Your cards:')
            console.log(showCards(player.cards))
            console.log('Your hand:')
            console.log(showCards(hands[player.hand]))
            console.log(`Your Puddings from past rounds: ${player.puddings}`)
            console.log("Opponent's cards:")
            console.log(showCards(ai.cards))
            console.log("Opponent's hand:")
            console.log(showCards(hands[ai.hand]))
            console.log(`Opponent's Puddings from past rounds: ${ai.puddings}`)
            if(player.cards.includes(11)){
                input = Object.values(await prompt.get("You have a chopsticks card. Wanna trade for any card of hand (yes/no)? "))[0]
                if(input === 'yes'){
                    getCard()
                    input = parseInt(Object.values(await prompt.get("Enter the number of the card you want "))[0])
                    const index = player.cards.indexOf(11)
                    player.cards.splice(index,1)
                    player.cards.push(hands[player.hand][input])
                    hands[player.hand].splice(input,1)
                    hands[player.hand].push(11)
                    
                }
            }
            getCard()
            input = parseInt(Object.values(await prompt.get("Enter the number of the card you want "))[0])
            const result = minimax(ai.cards,hands[ai.hand],ai.puddings,player.cards,hands[player.hand],player.puddings)
            if(typeof result[1] !=='number'){
                const index = ai.cards.indexOf(11)
                ai.cards.splice(index,1)
                ai.cards.push(hands[ai.hand][result[1][0]])
                hands[ai.hand].splice(result[1][0],1)
                hands[ai.hand].push(11)
                ai.cards.push(hands[ai.hand][result[1][1]])
                hands[ai.hand].splice(result[1][1],1)
            }
            else{
                ai.cards.push(hands[ai.hand][result[1]])
                hands[ai.hand].splice(result[1],1)
            }
            player.cards.push(hands[player.hand][input])
            hands[player.hand].splice(input,1)
            player.hand=(player.hand + 1) % 2
            ai.hand=(ai.hand + 1) % 2
        }
        console.log(`###### Round End ######`)
        console.log('Your cards:')
        console.log(showCards(player.cards))
        console.log(`Your opponent's cards:`)
        console.log(showCards(ai.cards))
        endTurn(player,ai)
        console.log(`Your points: ${player.points}`)
        console.log(`Your Puddings: ${player.puddings}`)
        console.log(`Opponent's points: ${ai.points}`)
        console.log(`Opponent's Puddings: ${ai.puddings}`)
    }
    console.log(`###### Game End ######`)
    if(player.puddings > ai.puddings){
        console.log(`You have the biggest quantity of Puddings! You got +6 points!`)
    }
    else if(player.puddings < ai.puddings){
        console.log(`Your opponent has the biggest quantity of Puddings! The AI got +6 points!`)
    }
    endGame(player,ai)
    console.log(`Your points: ${player.points}`)
    console.log(`Opponent's points: ${ai.points}`)
    if(player.points> ai.points){
        console.log(`You won!!!`)
    }
    else if(player.points < ai.points){
        console.log(`Your opponent won!!!`)
    }
    else{
        console.log(`Draw!!!`)
    }
}

function getCard(){
    for(let i = 0; i < hands[player.hand].length; i++){
        console.log(`${i} - ${showCards([hands[player.hand][i]])}`)
    }
}

play()