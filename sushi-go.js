const cards = [
    'Tempura',
    'Sashimi',
    'Dumpling',
    '3 Maki Rolls',
    '2 Maki Rolls',
    '1 Maki Roll',
    'Salmon Nigiri',
    'Squid Nigiri',
    'Egg Nigiri',
    'Pudding',
    'Wasabi',
    'Chopsticks'
]
function createDeck(){
    const cardDistribution = [14,14,14,12,8,6,10,5,5,10,6,4]
    const deck = []
    for(let i=0;i<cards.length;i++){
        for(let j=0;j<cardDistribution[i];j++){
            deck.push(i)
        }
    }
    return deck
}

function shuffle(deck){
    for (let i = deck.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = deck[i];
      deck[i] = deck[j];
      deck[j] = temp;
    }
  }

function draw(){
    if(typeof arguments[1] === 'number'){
        for(let i=2;i<arguments.length;i++){
            arguments[i].length = 0
            for(let j=0;j<arguments[1];j++){
                arguments[i].push(arguments[0].pop())
            }
        }
    }
    else{
        switch(arguments.length - 1){
            case 2:
                for(let i=1;i<arguments.length;i++){
                    arguments[i].length = 0
                    for(let j=0;j<10;j++){
                        arguments[i].push(arguments[0].pop())
                    }
                }
                break
            case 3:
                for(let i=1;i<arguments.length;i++){
                    arguments[i].length = 0
                    for(let j=0;j<9;j++){
                        arguments[i].push(arguments[0].pop())
                    }
                }
                break
            case 4:
                for(let i=1;i<arguments.length;i++){
                    arguments[i].length = 0
                    for(let j=0;j<8;j++){
                        arguments[i].push(arguments[0].pop())
                    }
                }
                break
            case 5:
                for(let i=1;i<arguments.length;i++){
                    arguments[i].length = 0
                    for(let j=0;j<7;j++){
                        arguments[i].push(arguments[0].pop())
                    }
                }
                break
        }
    }
}
function showCards(hand){
    let str = ''
    for(let i=0;i<hand.length;i++){
        if(i==hand.length - 1){
            str += `${cards[hand[i]]}`
        }
        else{
            str += `${cards[hand[i]]}, `
        }
    }
    return str
}
function endTurn(){
    let maxMakiP = []
    let secMaxMakiP = []
    let maxMaki = -Infinity
    let secMaxMaki = -Infinity
    for(let i=0;i<arguments.length;i++){
        let wasabi = 0
        let maki = 0
        let sashimi = 0
        let tempura = 0
        let dumpling = 0
        for(const c of arguments[i].cards){
            switch(c){
                case 0:
                    tempura++
                    if(tempura == 2){
                        tempura = 0
                        arguments[i].points += 5
                    }
                    break
                case 1:
                    sashimi++
                    if(sashimi == 3){
                        sashimi = 0
                        arguments[i].points += 10
                    }
                    break
                case 2:
                    if(dumpling < 5){
                        dumpling++
                        arguments[i].points += dumpling 
                    }
                    break
                case 3:
                    maki += 3
                    break
                case 4:
                    maki += 2
                    break
                case 5:
                    maki += 1
                    break
                case 6:
                    if(wasabi > 0){
                        arguments[i].points += 6
                    }
                    else{
                        arguments[i].points += 2
                    }
                    break
                case 7:
                    if(wasabi > 0){
                        arguments[i].points += 9
                    }
                    else{
                        arguments[i].points += 3
                    }
                    break
                case 8:
                    if(wasabi > 0){
                        arguments[i].points += 3
                    }
                    else{
                        arguments[i].points += 1
                    }
                    break
                case 9:
                    arguments[i].puddings++
                    break
                case 10:
                    wasabi++
                    break
                case 11:
                    break
            }
        }
        if(maki > maxMaki){
            secMaxMaki = maxMaki
            secMaxMakiP = maxMakiP
            maxMaki = maki
            maxMakiP = [i]
        }
        else if(maki == maxMaki){
            maxMakiP.push(i)
        }
        else if(maki > secMaxMaki){
            secMaxMaki = maki
            secMaxMakiP = [i]
        }
        else if(maki == secMaxMaki){
            secMaxMakiP.push(i)
        }
        arguments[i].cards = []
        arguments[i].hand = i
    }
    for(const p of maxMakiP){
        arguments[p].points += Math.floor(6 / maxMakiP.length)
    }
    if(maxMakiP.length == 1){
        for(const p of secMaxMakiP){
            arguments[p].points += Math.floor(3 / secMaxMakiP.length)
        } 
    }
    // minimax return for 2 players
    if(arguments[0].puddings - arguments[1].puddings > 0){
        return arguments[0].points - arguments[1].points + 6
    }
    else if(arguments[0].puddings - arguments[1].puddings < 0){
        return arguments[0].points - arguments[1].points - 6
    }
    else{
        return arguments[0].points - arguments[1].points
    }
    
}
function endGame(){
    let minPudding = Number.POSITIVE_INFINITY
    let minPuddingP = []
    let maxPudding = Number.NEGATIVE_INFINITY
    let maxPuddingP = []
    for(let i=0;i<arguments.length;i++){
        const puddings = arguments[i].puddings
        if(puddings > maxPudding){
            maxPudding = puddings
            maxPuddingP = [i]
        }
        else if(puddings == maxPudding){
            maxPuddingP.push(i)
        }
        if(puddings < minPudding){
            minPudding = puddings
            minPuddingP = [i]
        }
        else if(puddings == minPudding){
            minPuddingP.push(i)
        }
    }
    if(maxPuddingP.length !=arguments.length){
        for(const p of maxPuddingP){
            arguments[p].points += Math.floor(6 / maxPuddingP.length)
        }
        if(arguments.length > 2){
            for(const p of minPuddingP){
                arguments[p].points -= Math.floor(6 / minPuddingP.length)
            }
        }
    }
}
function minimax(maxCards,maxHand,maxPuddings,minCards,minHand,minPuddings,maxDepth = Number.POSITIVE_INFINITY,maxPlayer = true,alpha = Number.NEGATIVE_INFINITY,beta = Number.POSITIVE_INFINITY,depth = 0,chopsticks = false){

    if(depth == maxDepth || (maxHand.length == 0 && minHand.length == 0)){
        const value = endTurn({cards:maxCards,puddings:maxPuddings,points:0},
            {cards:minCards,puddings:minPuddings,points:0}
            )
        return [value, -1]
    }
    else{
        if(maxPlayer){
            let maxValue = [Number.NEGATIVE_INFINITY,-1]
            for(let card = 0; card < maxHand.length; card++){
                const cardsClone = maxCards.slice(0)
                const handClone = maxHand.slice(0)
                cardsClone.push(handClone[card])
                handClone.splice(card,1)
                const value = minimax(
                    cardsClone,
                    handClone,
                    maxPuddings,
                    minCards,
                    minHand,
                    minPuddings,
                    maxDepth,
                    false,
                    alpha,
                    beta,
                    depth,
                    false
                    )
                if(value[0] > maxValue[0]){
                    maxValue = [value[0], card]
                }
                alpha = Math.max(alpha,maxValue[0]) 
                if(beta <=alpha){
                    break 
                }
            }
            if(maxCards.includes(11) && !chopsticks){
                for(let card = 0; card < maxHand.length; card++){
                    const cardsClone = maxCards.slice(0)
                    const handClone = maxHand.slice(0)
                    const index = cardsClone.indexOf(11)
                    cardsClone.splice(index,1)
                    cardsClone.push(handClone[card])
                    handClone.push(11)
                    handClone.splice(card,1)
                    const value = minimax(
                        cardsClone,
                        handClone,
                        maxPuddings,
                        minCards,
                        minHand,
                        minPuddings,
                        maxDepth,
                        true,
                        alpha,
                        beta,
                        depth,
                        true,
                        )
                    if(value[0] > maxValue[0]){
                        maxValue = [value[0], [card,value[1]]]
                    }
                    alpha = Math.max(alpha,maxValue[0])
                }
            }
            return maxValue
        }
        else{
            let minValue = [Number.POSITIVE_INFINITY,-1]
            for(let card = 0; card < minHand.length; card++){
                const cardsClone = minCards.slice(0)
                const handClone = minHand.slice(0)
                cardsClone.push(handClone[card])
                handClone.splice(card,1)
                const value = minimax(
                    maxCards,
                    handClone,
                    maxPuddings,
                    cardsClone,
                    maxHand,
                    minPuddings,
                    maxDepth,
                    true,
                    alpha,
                    beta,
                    depth + 1,
                    false
                    )
                if(value[0] < minValue[0]){
                    minValue = [value[0], card]
                }
                beta = Math.min(beta,minValue[0])
                if(beta <= alpha){
                    break 
                }
            }
            if(minCards.includes(11) && !chopsticks){
                for(let card = 0; card < minHand.length; card++){
                    const cardsClone = minCards.slice(0)
                    const handClone = minHand.slice(0)
                    const index = cardsClone.indexOf(11)
                    cardsClone.splice(index,1)
                    cardsClone.push(handClone[card])
                    handClone.splice(card,1)
                    handClone.push(11)
                    const value = minimax(
                        maxCards,
                        maxHand,
                        maxPuddings,
                        cardsClone,
                        handClone,
                        minPuddings,
                        maxDepth,
                        false,
                        alpha,
                        beta,
                        depth,
                        true,
                        )
                    if(value[0] < minValue[0]){
                        minValue = [value[0], [card,value[1]]]
                    }
                    beta = Math.min(beta,minValue[0])
                }
            }
            return minValue
        }
    }
}
module.exports =  {createDeck,shuffle,draw,showCards,endTurn,endGame,minimax}